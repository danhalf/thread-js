import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel }) {
    super(postReactionModel);
  }

  getPostReaction({ userId, isLike }, postId) {
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ postId, isLike })
      .withGraphFetched('[post]')
      .first();
  }
}

export { PostReaction };
