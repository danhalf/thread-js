const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  UPDATE: '/:id/update',
  DELETE: '/:id/delete'
};

export { PostsApiPath };
