import { useCallback, useDispatch, useEffect, useSelector, useState } from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { threadActionCreator } from 'store/actions';
import { image as imageService } from 'services/services';
import { Checkbox, Post, Spinner } from 'components/common/common';
import { AddPost, ExpandedPost, SharedPostLink } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  isActive: true,
  from: 0,
  count: 10
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id,
    postCreatorId: state.posts.postCreatorId
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);

  const handlePostLike = useCallback(
    ({ id, isLiked }) => dispatch(threadActionCreator.likePost({ id, isLiked })),
    [dispatch]
  );

  const handlePostBodyUpdate = useCallback(postUpdateData => (
    dispatch(threadActionCreator.updatePostBody(postUpdateData))
  ), [dispatch]);

  const handlePostDelete = useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const toggleShowLikedPost = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.userId = showLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          isChecked={showOwnPosts}
          label="Show only my posts"
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          isChecked={showLikedPosts}
          label="Show only my liked posts"
          onChange={toggleShowLikedPost}
        />
      </div>
      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            onPostDelete={handlePostDelete}
            onPostBodyUpdate={handlePostBodyUpdate}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
