import { Abstract } from '../abstract/abstract.repository';
import {
  getCommentsCountQuery,
  getReactionsQuery,
  getWherePostActiveQuery,
  getWhereUserIdQuery
} from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      isActive,
      userId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
        // getWhoReactQuery(this.model)
      )
      .where(getWhereUserIdQuery(userId, isActive))
      .where(getWherePostActiveQuery(isActive))
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .first();
  }

  updatePostById(postId, dataToUpdate) {
    return this.model.query().patchAndFetchById(postId, dataToUpdate);
  }
}

export { Post };
