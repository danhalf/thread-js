class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async updatePost(userId, { postId, postBody, postImage }) {
    const currentPost = await this.getPostById(postId);
    const { user } = currentPost;
    const isPostAuthor = user.id === userId;
    return isPostAuthor ? this._postRepository.updatePostById(postId, { body: postBody, imageId: postImage }) : false;
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      { userId, isLike },
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction({ userId, isLike }, postId);
  }

  async setPostToInactive(userId, { postId, postActive = false }) {
    const currentPost = await this.getPostById(postId);
    const { user } = currentPost;
    const isPostAuthor = user.id === userId;
    return isPostAuthor ? this._postRepository.updatePostById(postId, { isActive: postActive }) : false;
  }
}

export { Post };
