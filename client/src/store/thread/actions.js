import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    await services.post.deletePost(postId);
    return { posts };
  }
);

const toggleExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const updatePostBody = createAsyncThunk(
  ActionType.UPDATE_POST,
  async (postDataPayload, { getState, extra: { services } }) => {
    await services.post.updatePostBody(postDataPayload);
    const {
      posts: { posts, expandedPost }
    } = getState();

    return { posts, expandedPost };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (payload, { getState, extra: { services } }) => {
    const [postId, isLiked] = Object.values(payload);
    const { id, isLike } = await services.post.likePost({ postId, isLiked });
    let currentReaction = ({ likeCount: 0, dislikeCount: 0 });
    if (id && isLike === true) currentReaction = ({ likeCount: 1, dislikeCount: 0 });
    if (id && isLike === false) currentReaction = ({ likeCount: 0, dislikeCount: 1 });

    const { likeCount, dislikeCount } = currentReaction;

    const mapLikes = post => ({
      ...post,
      likeCount,
      dislikeCount
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    const updatedExpandedPost = expandedPost?.id === postId
      ? mapLikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== comment.postId ? post : mapComments(post)
    ));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePostBody,
  deletePost,
  toggleExpandedPost,
  likePost,
  addComment
};
