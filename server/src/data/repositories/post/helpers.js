const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';
  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getWhoReactQuery = model => model.relatedQuery('whoPostReact').as('whoPostReact');

const getWhereUserIdQuery = userId => builder => {
  if (userId) {
    builder.where({ userId });
  }
};

const getWherePostActiveQuery = isActive => builder => {
  if (isActive) {
    builder.where({ isActive });
  }
};

export { getCommentsCountQuery, getReactionsQuery, getWhereUserIdQuery, getWherePostActiveQuery, getWhoReactQuery };
