import PropTypes from 'prop-types';
import { useState } from 'react';

import { getFromNowTime } from 'helpers/helpers';
import { IconName } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { IconButton, Image } from 'components/common/common';

import styles from './styles.module.scss';
import Modal from '../modal/modal';
import UpdatePost from '../../thread/components/update-post/update-post';
import { useSelector } from '../../../hooks/hooks';

const Post = ({ post, onPostLike, onExpandedPostToggle, sharePost, onPostBodyUpdate, onPostDelete }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;

  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));

  const date = getFromNowTime(createdAt);
  const [isModalOpened, setIsModalOpened] = useState(false);
  const isAuthor = userId === user.id;

  const handlePostLike = event => {
    const dataIconName = event.currentTarget.getAttribute('attribute');
    const isLiked = dataIconName === 'like';
    onPostLike({ id, isLiked });
  };

  const handlePostUpdateModal = () => {
    setIsModalOpened(true);
  };

  const handlePostBodyUpdate = payload => {
    onPostBodyUpdate({ id, postBody: payload.body, postImageId: payload.image });
  };

  const handlePostDelete = () => {
    onPostDelete(id);
  };

  const handleExpandedPostToggle = () => onExpandedPostToggle(id);

  return (
    <div className={styles.card}>
      {isModalOpened && (
        <Modal isOpen isCentered onClose={() => setIsModalOpened(false)}>
          <UpdatePost onPostUpdate={handlePostBodyUpdate} postBody={body} currentPostImageLink={image?.link} />
        </Modal>
      )}
      {image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>{`posted by ${user.username} - ${date}`}</span>
        </div>
        <p className={styles.description}>{body}</p>
      </div>
      <div className={styles.extra}>
        <IconButton
          iconName={IconName.THUMBS_UP}
          attribute="like"
          label={likeCount}
          onClick={handlePostLike}
        />
        <IconButton
          iconName={IconName.THUMBS_DOWN}
          attribute="dislike"
          label={dislikeCount}
          onClick={handlePostLike}
        />
        <IconButton
          iconName={IconName.COMMENT}
          label={commentCount}
          onClick={handleExpandedPostToggle}
        />
        <IconButton
          iconName={IconName.SHARE_ALTERNATE}
          onClick={() => sharePost(id)}
        />
        { isAuthor && (
          <div className={styles.control}>
            <IconButton
              class
              iconName={IconName.EDIT}
              onClick={handlePostUpdateModal}
            />
            <IconButton
              class
              iconName={IconName.TRASH}
              onClick={handlePostDelete}
            />
          </div>
        ) }
      </div>
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostBodyUpdate: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
