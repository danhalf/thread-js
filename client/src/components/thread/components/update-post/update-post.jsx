import { useCallback, useState } from 'hooks/hooks';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'common/enums/enums';
import { Button, Image, TextArea, Segment } from 'components/common/common';

import styles from './styles.module.scss';

const UpdatePost = ({ postBody, currentPostImageLink, onPostUpdate, uploadImage }) => {
  const [body, setBody] = useState(postBody);
  const [image, setImage] = useState(currentPostImageLink);
  const [isUploading, setIsUploading] = useState(false);

  const handleUpdatePost = async ev => {
    ev.preventDefault();
    if (!body) {
      return;
    }
    await onPostUpdate({ image, body });
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleTextAreaChange = useCallback(ev => setBody(ev.target.value), [setBody]);
  return (
    <Segment>
      <form onSubmit={handleUpdatePost}>
        <TextArea
          name="body"
          value={body}
          placeholder="post text"
          onChange={handleTextAreaChange}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post image" />
          </div>
        )}
        {currentPostImageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={currentPostImageLink} alt="post image" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >

            <label className={styles.btnImgLabel}>
              Change image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Update
          </Button>
        </div>
      </form>
    </Segment>
  );
};

UpdatePost.propTypes = {
  postBody: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  currentPostImageLink: PropTypes.string.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatePost;
