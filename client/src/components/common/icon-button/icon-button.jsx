import PropTypes from 'prop-types';

import { IconName } from 'common/enums/enums';
import Icon from '../icon/icon';

import styles from './styles.module.scss';

const IconButton = ({
  iconName,
  label,
  onClick,
  attribute = null
}) => (
  <button className={styles.iconButton} type="button" onClick={onClick} attribute={attribute}>
    <Icon name={iconName} />
    {label}
  </button>
);

IconButton.propTypes = {
  iconName: PropTypes.oneOf(Object.values(IconName)).isRequired,
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClick: PropTypes.func.isRequired,
  attribute: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

IconButton.defaultProps = {
  label: '',
  attribute: null
};

export default IconButton;
